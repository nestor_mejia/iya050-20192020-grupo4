const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  res.header('Access-Control-Allow-Methods', 'GET,POST,PATCH,DELETE');
  res.header('Access-Control-Allow-Origin', '*');
  next();
});

const database = {}
const read = (id) => {
  return database[id]
}
const readAll = () => {
  return Object.values(database)
}
const update = (id, item) => {
  database[id] = { id, ...item }
  return item
}
const create = update
const remove = (id) => {
  delete database[id]
  return item
}

app.post('/', (req, res) => {
  try {
    console.log(req.body)
    const query = req.body
    if (!query.action) throw new Error('bad request')

    switch(query.action) {
      case 'read': {
        res.send(JSON.stringify(read(query.id)))
        return
      }
      case 'read-all': {
        res.send(JSON.stringify(readAll()))
        return
      }
      case 'create': {
        create(query.id, query.item)
        return res.send('OK')
      }
      case 'update': {
        update(query.id, query.item)
        return res.send('OK')
      }
      case 'delete': {
        remove(query.id)
        return res.send('OK')
      }
      default: throw new Error('unknown action')
    }
  } catch(error) {
    console.error(error)
    res.status(500).send('Internal error')
  }
});

app.listen(6000, () => console.log(`Listening on port 6000`));
