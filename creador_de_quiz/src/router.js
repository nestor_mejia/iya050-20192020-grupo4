// https://stackoverflow.com/questions/40764596/using-react-router-with-cdn-and-without-webpack-or-browserify
"use strict";

const Link = ReactRouterDOM.Link,
  Route = ReactRouterDOM.Route,
  withRouter = ReactRouterDOM.withRouter;

console.log(ReactRouterDOM);

let Questions = [];

class AddQuestions extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      id: new Date().getTime(),
      question: "",

      answers: []
    };
    this.handleImput = this.handleImput.bind(this);
    this.addItem = this.addItem.bind(this);
  }

  componentDidMount() {
    this.addNewAnswer();
    this.addNewAnswer();
  }

  handleImput(e) {
    this.setState({ question: e.target.value });
  }

  addItem() {
    Questions.push(this.state);
  }

  addNewAnswer() {
    const id = new Date().getTime();
    this.setState(oldState => ({
      answers: [...oldState.answers, { text: "", id }]
    }));
  }

  onAnwserChange(key, e) {
    const answers = this.state.answers;

    answers[key].text = e.target.value;

    this.setState({ answers });

    //const newanswers =
  }

  render() {
    console.log(this.state, window.Questions);

    return (
      <header>
        <div className="addQuestionsForm">
          {/* <div className="questionDiv"> */}
          <label className="questionLabel">Escribe Tu Pregunta: </label>

          <input
            type="text"
            placeholder="Escribe tu pregunta"
            value={this.state.question}
            onChange={this.handleImput}
          ></input>
          {/* </div> */}

          <div className="divBtns">
            <button onClick={() => this.addItem()} type="submit">
              Agregar
            </button>

            <button onClick={() => this.addNewAnswer()}>
              Agregar mas respuestas
            </button>
          </div>

          <div className="elementsList">
            {this.state.answers.map((ans, key) => (
              <input
                type="text"
                key={key}
                onChange={e => this.onAnwserChange(key, e)}
                placeholder={`Respuesta ${key + 1}`}
              />
            ))}
          </div>
        </div>
      </header>
    );
  }
}

class ListElements extends React.Component {
    constructor() {
        super();
        this.state = { time: 4 };
        this.clock;
      }
    
  getQuestion() {
    return Questions[0];
  }
  componentDidMount() {
    this.clock = setInterval(() => {
      const time = this.state.time - 1;
      if (time == 0) {
        console.log(this.props.history.push("/play"));
        clearInterval(this.clock);
      }

      this.setState({ time });
    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.clock);
  }

  render() {
    <h2>{`Tiempo restante ${this.state.time}`}</h2>;
    const question = this.getQuestion();

    if (!question) return <h1>Nel</h1>;

    return (
      <div className="center">
        <div className="questionHeader">
          <h1>{question.question}</h1>
        </div>

        {question.answers.map((ans, key) => (
          <Link key={key} to={`/feedback/${question.id}/${ans.id}`}>
            <div className="answers">{ans.text}</div>
          </Link>
        ))}
      </div>
    );
  }
}

class FeedBack extends React.Component {
  constructor() {
    super();
    this.state = { time: 4 };
    this.clock;
  }

  componentDidMount() {
    this.clock = setInterval(() => {
      const time = this.state.time - 1;
      if (time == 0) {
        console.log(this.props.history.push("/play"));
        clearInterval(this.clock);
      }

      this.setState({ time });
    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.clock);
  }

  getQuestion() {
    return Questions[0];
  }

  render() {
    console.log(this.props);

    const question = this.getQuestion();

    return <h2>{`Tiempo restante ${this.state.time}`}</h2>;
    
    if (!question) return <h1>Nel</h1>;

    return (
      <div>
        <h1>{question.question}</h1>

        <h1>{`Tiempo restante ${this.state.time}`}</h1>

        {question.answers.map((ans, key) => (
          <Link key={key} to={`/feedback/${question.id}/${ans.id}`}>
            {ans.text}
          </Link>
        ))}
      </div>
    );
  }
}

class WelcomePage extends React.Component {
  render() {
    return (
      <div className="center">
        <div className="header">
          <h1>Nelhoot</h1>
        </div>
    </div>
    );
  }
}

const App = props => (
  <ReactRouterDOM.HashRouter>
    <div className="divButtons">
      {/* <div className="divBtn"> */}
        <button className="btn b">
          <Link to="/">Inicio</Link>
        </button>
      {/* </div> */}
      {/* <div className="divBtn"> */}
        <button className="btn a">
          <Link to="/addQuestions">Agregar Preguntas</Link>
        </button>
      {/* </div> */}
      {/* <div className="divBtn"> */}
        <button className="btn b">
          <Link to="/play">!A Jugar!</Link>
        </button>
      {/* </div> */}
    </div>

    <Route path="/" exact component={withRouter(WelcomePage)} />
    <Route path="/addQuestions" component={AddQuestions} />
    <Route path="/play" component={ListElements} />
    <Route path="/feedback/:question_id/:answer_id" component={FeedBack} />
  </ReactRouterDOM.HashRouter>
);

ReactDOM.render(<App />, document.getElementById("questionsForm"));
