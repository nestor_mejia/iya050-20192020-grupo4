const { React, ReactDOM } = window;

class MemeView extends React.Component {
  constructor(props) {
    super(props);
    this.state = { dailyMeme: null, daily: false };
  }

  componentDidMount = async () => {

    if(this.props.location.state.daily){
      this.setState({daily : true});
    } 

    this.getMeme("http://localhost:4000/graphql");

  };

  getMeme = sourceUrl => {


    const request = this.props.location.state.daily ? `{ getDailyMeme{ id, name, url, caption } }` : `{ getMeme(id: ${this.props.location.state.memeId}){ id, name, url, caption } }`;

    fetch(sourceUrl,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
        body: JSON.stringify({query: request})
      }
      )
      .then(res => res.json())
      .then(response => {

        const meme = this.props.location.state.daily ? response.data.getDailyMeme : response.data.getMeme;

        this.setState({ dailyMeme: meme });
      })
      .catch(e => {
        console.error(e);
      });
  };

  render() {
    if (!this.state.dailyMeme) {
      return <div>Loading</div>;
    }

    return (
      <div className="totally-centered">
        <h1>{this.state.daily ? "Daily Meme" : this.state.dailyMeme.name}</h1>
        <Meme image={this.state.dailyMeme.url} {...this.state.dailyMeme}></Meme>
      </div>
    );
  }
}
