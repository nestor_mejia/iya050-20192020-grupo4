const { React, ReactDOM } = window;

class MemeForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        url: null,
        name: null,
        caption: null,
      }
    };
  }

  sendForm = e => {
    e.preventDefault();

    const mutation =`mutation postMeme($memeInput: MemeInput!){
            postMeme( memeInput: $memeInput) { name, caption, url }
          }
      `;

    fetch("http://localhost:4000/graphql", {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({
        query : mutation,
        variables : { memeInput : this.state.form}
      })
    })
      .then(() => alert("Meme created"))
      .catch(() => alert("Problem uploading meme"));
  };

  onFormChange = e => {
    const id = e.target.id,
      value = e.target.value;

    this.setState(prevState => ({
      form: {
        ...prevState.form,
        [id]: value
      }
    }));
  };

  render() {
    return (
      <div style={{ width: "90%" }}>
        <h1>Add a meme!</h1>
        <form className="add-form">
          <div>
            <label htmlFor="name">Meme name: </label>
            <input type="text" id="name" onChange={this.onFormChange} />
          </div>
          <div>
            <label htmlFor="name">Meme caption: </label>
            <input type="text" id="caption" onChange={this.onFormChange} />
          </div>
          <div>
            <label htmlFor="name">Meme image url: </label>
            <input type="text" id="url" onChange={this.onFormChange} />
          </div>
          <button type="submit" onClick={this.sendForm}>
            Send
          </button>
        </form>
      </div>
    );
  }
}
