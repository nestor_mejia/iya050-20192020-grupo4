const { React, ReactDOM } = window;

class MemesList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      memes: []
    };
  }
  componentDidMount = async () => {
    this.getMemes("http://localhost:4000/graphql",);
  };

  getMemes = sourceUrl => {
    fetch(sourceUrl,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
        body: JSON.stringify({query: "{ getMemes{ id, name, url, caption } }"})
      })
      .then(res => res.json())
      .then(response => {
        console.log(response);
        this.setState({ memes: response.data.getMemes });
      })
      .catch(e => {
        console.error(e);
      });
  };

  render() {
    return (
      <div className="body-content">
        <h1>Memes</h1>
        {this.state.memes.map(meme => {
          return (
            <Meme
              key={meme.id}
              image={meme.url}
              {...meme}
              onClick={() => this.props.history.push("/meme-view", { memeId : meme.id })}
            ></Meme>
          );
        })}
      </div>
    );
  }
}
