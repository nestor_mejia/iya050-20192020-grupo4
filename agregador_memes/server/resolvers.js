const fetch = require('node-fetch');

const DATASTORE = "https://0qh9zi3q9g.execute-api.eu-west-1.amazonaws.com/development";
const APPLICATION_ID = "nestor.mejia";

const ERROR_RESPONSE = {
    id: 404,
    name: "Cannot retrieve your memes:(",
    caption: "Sorry!",
    url: "https://www.mememaker.net/api/bucket?path=static/img/memes/full/2014/Jul/11/15/im-sorry.-please-dont-hate-me.jpg"
};

const callDataStore = async(isUpdating, requestBody) => {

    const requestOptions = {
        headers: {
            'Content-Type': 'application/json',
            'x-application-id': APPLICATION_ID
        }
    }

    if (isUpdating) {
        requestOptions.method = "PUT";
        requestOptions.body = requestBody;
    }

    return fetch(`${DATASTORE}/pairs/memesList`, requestOptions)
        .then(res => res.json())
        .then((response) => {

            let data;

            if (response.value) {
                data = JSON.parse(response.value);
            } else {
                data = {
                    error: "Error ocurred while fetching data",
                    code: 500
                };
            }
            return data;
        })
        .catch((e) => {
            console.error(e)
            return {
                error: "Error ocurred while fetching data",
                code: 500
            };
        });

}

module.exports.RESOLVERS = {

    getMemes: async() => {

        const memes = await callDataStore(false);

        if (memes.error) {
            return [ERROR_RESPONSE];
        }

        return memes;

    },

    getDailyMeme: async() => {

        const memes = await callDataStore(false);

        if (memes.error) {
            return ERROR_RESPONSE;
        }

        return memes[Math.floor(Math.random() * memes.length)];

    },

    getMeme: async({ id }) => {

        const memes = await callDataStore(false);

        if (memes.error) {
            return ERROR_RESPONSE;
        }

        const memeResponse = memes.find(meme => meme.id === id);

        return memeResponse;
    },

    postMeme: async({ memeInput }) => {

        const memes = await callDataStore(false);

        if (memes.error) {
            return ERROR_RESPONSE;
        }

        const newMeme = {
            id: memes.length + 1,
            url: memeInput.url,
            name: memeInput.name,
            caption: memeInput.caption
        };

        memes.push(newMeme);

        const memesListNewValue = JSON.stringify(memes);

        const memesUpdated = await callDataStore(true, memesListNewValue);

        return memesUpdated[memesUpdated.length - 1];

    }

};