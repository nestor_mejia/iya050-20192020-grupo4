module.exports.SCHEMA = `

type Meme { 
    id: Int!,
    url: String!
    name: String!,
    caption: String!
  }

  type Error{
    error : String!,
    code: Int!
  }
  
  input MemeInput{
      url: String!,
      name: String!,
      caption: String!
  }

type Query {
  getMeme(id: Int!): Meme,
  getMemes: [Meme],
  getDailyMeme: Meme
}

type Mutation {
    postMeme(memeInput: MemeInput!) : Meme
}


`