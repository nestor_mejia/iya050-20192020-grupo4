const express = require('express');
const graphqlHTTP = require('express-graphql');
const { buildSchema } = require('graphql');
const cors = require('cors');

const { SCHEMA } = require('./schema');
const { RESOLVERS } = require('./resolvers');

const schema = buildSchema(SCHEMA);

const app = express();
//CORS POLICY
app.use(cors());

app.use('/graphql', graphqlHTTP({
    schema: schema,
    rootValue: RESOLVERS,
    graphiql: true,
}));

app.listen(4000, () => console.log("App running on port 4000"));