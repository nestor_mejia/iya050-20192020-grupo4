const { React, ReactDOM } = window;
const { BrowserRouter: Router, Link, Switch, Route, withRouter, Redirect  } = window.ReactRouterDOM;

class Index extends React.Component {

  constructor(props) {
    super(props);

  }

  render() {
    return (
      <Router>
        <Header />
        <div className="body-container">
          <SideBar />
          <Switch className="body-content">
            <Route path="/news-list" component={withRouter(NewsList)}/>
            <Route path="/news-view" component={withRouter(NewsView)}/>
            <Route path="/news-form" component={withRouter(NewsForm)}/>
            <Route path="/" component={withRouter(Home)}/>
          </Switch>
        </div>
      </Router>
    );
  }
}

ReactDOM.render(<Index />, document.getElementById("root"));