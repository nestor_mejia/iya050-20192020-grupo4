const { React, ReactDOM } = window;

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = { };
  }

  render() {
    return (
      <div className="header-container">
        <div className="header-text">
          <img className="header-image" src="assets/images/generatedtext.png"/>
        </div>
      </div>
    );
  }

}
