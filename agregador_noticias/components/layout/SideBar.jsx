const logos = [
  "https://cdn.pixabay.com/photo/2015/05/17/10/51/facebook-770688_960_720.png",
  "https://1000marcas.net/wp-content/uploads/2019/11/Logo-Twitter.png",
  "https://upload.wikimedia.org/wikipedia/commons/thumb/5/58/Instagram-Icon.png/600px-Instagram-Icon.png"
];

const links = [
  "https://facebook.com",
  "https://twitter.com",
  "https://instagram.com"
];
class SideBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = { userName: "", email: "", isSubmitting: false };
  }

  renderLogos = () =>{
    const logosList = []
    for(let i = 0; i<links.length; i++){
      logosList.push(
        <a href={links[i]} target="_blank">
          <div className="side-social-container">
          <img className="side-social-logo" src={logos[i]} />
          </div>
        </a>
      );
    }
    return logosList;
  }

  render() {
    return (
      <div className="side-container">
        <div className="side-text">
          <p>Nuestras redes sociales</p>
          {this.renderLogos()}
        </div>
      </div>
    );
  }

}