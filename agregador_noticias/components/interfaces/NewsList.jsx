class NewsList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false,
      news: []
    };
  }

  componentDidMount = async () => {
    this.getNewsFromStub("http://localhost/WebProgrammingII/agregador_noticias/news.json");
  }

  getNewsFromRemoteServer = (url) => {
    //when remote server is available
  }

  getNewsFromStub = (stubUri) => {
    fetch(stubUri)
      .then((res) => res.json())
      .then((response) => {
        this.setState({ loaded: true, news: response.news });
      })
      .catch((e) => {
        console.error(e);
      });
  }

  renderCards = () => {
    let news = [];
    this.state.news.map((element) => {
      news.push(
        <NewsCard
          key = {element.id}
          newsInfo={element}
        />
      );
    });

    return news;
  }

  render() {
    return (
      <div className={"centering news-container"}>
        {this.state.loaded ? this.renderCards() : <p>No data</p>}
        
      </div>
    );
  }

}