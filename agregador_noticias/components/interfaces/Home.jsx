class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            navigateTo : "",
            navigate : false
        };
        this.onNavigationStarted = this.onNavigationStarted.bind(this);
    }

    onNavigationStarted = (event) => {
        this.setState({navigateTo : event.target.getAttribute("to"), navigate: true});
    }

    render() {
        return (
            <div className="body-content">
                <div className={"body-content home-container"}>
                    <div className={"centering home-hover-container"}>
                        <div className={"centering home-hover-info"}>
                            <p className={"centering home-hover-info-welcome"}>Bienvenido al portal de noticias SportsNews</p>
                            <hr />
                            <div className={"centering home-hover-button"} to="/news-list" onClick={this.onNavigationStarted}>Ir a las noticias</div>
                            <div className={"centering home-hover-button"} to="/news-form" onClick={this.onNavigationStarted}>Solicitar redacción de noticia</div>
                        </div>
                    </div>
                </div>
            {this.state.navigate ? <Redirect push to={this.state.navigateTo} /> : <p></p>}
            </div>

        );
    }

}