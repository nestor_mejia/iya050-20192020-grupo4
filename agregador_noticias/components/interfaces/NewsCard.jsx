class NewsCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            navigate : false,
            seen : false
        };
    }

    render() {
        return (
            <div className="card-container" onClick={()=>{this.setState({seen:true, navigate: true})}}>
                <div className="card-title">
                    <p>{this.props.newsInfo.header}</p>
                </div>
                <div className="card-image">
                    <img className="card-image-dimensions" src={this.props.newsInfo.img} />
                </div>
                <div className="card-description">
                    <p>{this.props.newsInfo.lede}</p>
                </div>
                {this.state.seen ? <p>Noticia vista</p> : <p></p>}
                {this.state.navigate ? <Redirect push to={{ pathname: "/news-view", state : this.props.newsInfo}}/> : <p></p>}
            </div>
        );
    }

}