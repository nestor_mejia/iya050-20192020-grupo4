const labels = [
  "Título de la noticia",
  "Autor",
  "Fecha",
  "Imagen",
  "Lead",
  "Cuerpo",
  "Final"
];

const formFields = [
  "header",
  "author",
  "date",
  "img",
  "lede",
  "body",
  "footer"
];

class NewsForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newsInfo: {},
      header: "",
      author: "",
      date: "",
      img: "",
      lede: "",
      body: "",
      footer: "",
      isSubmitting: false
    };
    this.onInputChange = this.onInputChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    
  }

  renderFields = () => {
    let fields = [];

    for (let i = 0; i < labels.length; i++) {
      fields.push(
        <label className="newsform-label"  key={formFields[i]}>
          {labels[i]}
          <br />
          <input
          key={formFields[i]}
            type="text"
            name={formFields[i]}
            value={this.state[formFields[i]]}
            onChange={this.onInputChange}
            required
          />
          <br/>
        </label>
      );
    }

    return fields;

  }

  onInputChange(event) {
    const target = event.target;
    this.setState({
      [target.name]: target.value
    });
  }

  onSubmit() {

    const newsInfoForm = {
      header: this.state.header,
      author: this.state.author,
      date: this.state.date,
      img: this.state.img,
      lede: this.state.lede,
      body: this.state.body,
      footer: this.state.footer,
    };

    this.setState({ newsInfo: newsInfoForm });

    /*loading*/
    setTimeout(() => {
      this.setState({ isSubmitting: true });
    }, 1000);

    event.preventDefault();
    //Fetch to remote server

  }


  render() {
    return (
      <div className={"centering newsform-container"}>
        <form onSubmit={this.onSubmit}>
          {this.renderFields()}
          <input
            type="submit"
            value="Enviar y ver vista previa"
            disabled={this.state.isSubmitting}
          />
        </form>
        {this.state.isSubmitting ? <Redirect push to={{ pathname: "/news-view", state: this.state.newsInfo }} /> : <p></p>}
      </div>
    );
  }

}