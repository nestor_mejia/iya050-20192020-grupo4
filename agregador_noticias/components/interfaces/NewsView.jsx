class NewsView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newsInfo: {}
    };
  }

  componentDidMount = () => {
    this.setState({ newsInfo: this.props.location.state });
  }

  render() {
    console.log(this.state.newsInfo);
    return (
      <div className={"centering body-content newsview-container"}>
        <div className="newsview-title-container">
          <p className={"newsview-title-text"}>{this.state.newsInfo.header}</p>
        </div>
        <div className="newsview-lede-container">
          <p className={"newsview-lede-text"}>{this.state.newsInfo.lede}</p>
        </div>
        <div className="newsview-redaction-container">
          <p className="newsview-redaction-text">{this.state.newsInfo.author}. {this.state.newsInfo.date}</p>
        </div>
        <div className="newsview-img-container">
          <img className="newsview-img-dimensions" src={this.state.newsInfo.img} />
        </div>
        <div className="newsview-body-container">
          <p className="newsview-body-text">{this.state.newsInfo.body}</p>
        </div>
        <div className="newsview-footer-container">
          <p className="newsview-body-text">{this.state.newsInfo.footer}</p>
        </div>
      </div>
    );
  }

}